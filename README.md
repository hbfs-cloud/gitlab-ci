# gitlab-ci

Free Gitlab-ci template library provided by HBFS-Cloud company inspired by :

 - https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates
 - https://r2devops.io/
 - https://gitlab.com/lydra/gitlab-ci-templates
 - https://dev.to/anhtm/a-comprehensive-guide-to-creating-your-own-gitlab-ci-template-library-5b3i

## Getting started

Simply include the template and customize behavior using variables :

```
  - remote: 'https://gitlab.com/hbfs-cloud/gitlab-ci/-/blob/main/security/sbom.yml'
```

## Structure

Backbone structure is based on DevOps keywords <img src="devops.png" alt="Hierarchy" width="20%" height="20%"> :

 - audit
 - build
 - code
 - communicate
 - deploy
 - document
 - monitor
 - operate
 - plan
 - release
 - security
 - test